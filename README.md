Ansible Role: Composer
===================

Install Composer on Debian servers.
It will download the `composer.setup` from [getcomposer.org/installer](https://getcomposer.org/installer) and install Composer in `composer_inst_path`

----------
Requirements
-------------------
This role require PHP installed on the target machine. You can use this role just after the role [`php-fpm`](https://bitbucket.org/elevenlab/ansible-php-fpm) which provides PHP

Role variables
-------------

Available variables are listed below, along with default values (see `defaults/main.yml`):

	composer_inst_path: "/usr/local/bin"

The directory where composer will be installed

Dependencies
-------------------
None.

Example Playbook
--------------------------
	- hosts: webservers
	  roles:
	    - phprole
		- composer
